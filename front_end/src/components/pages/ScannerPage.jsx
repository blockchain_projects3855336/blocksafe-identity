import React, { useState, useEffect } from "react";
import { Box, Paper, Typography, Button } from "@mui/material";
import bgImg from "../../img/bg.png";
import QrScannerComponent from "../QrScanner";
import useAuth from "../../hooks/useAuth";
import { useNavigate } from "react-router-dom";
import { contractAddress as CONTRACT_ADDRESS } from "../../utils/constants";

const ScannerPage = () => {
  const [qrData, setQrData] = useState("");
  const { auth } = useAuth();
  const navigate = useNavigate();

  const passData = (data) => {
    setQrData(data);
    console.log("qrData:", data);
  };

  useEffect(() => {
    if (!qrData) return;

    console.log("auth:", auth);
    console.log("qrData:", qrData);

    if (qrData.text && typeof qrData.text === "string") {
      const arr = qrData.text.split(",");
      const contractAddress = arr[0];

      if (contractAddress) {
        if (contractAddress === CONTRACT_ADDRESS) {
          if (auth.role === "supplier" || auth.role === "retailer") {
            navRole();
          } else {
            navUser();
          }
        } else {
          navFakeProduct();
        }
      }
    }
  }, [qrData, auth]);

  const navigateTo = (path) => {
    navigate(path);
  };

  const navRole = () => {
    navigateTo("/update-product");
  };

  const navUser = () => {
    navigateTo("/authentic-product");
  };

  const navFakeProduct = () => {
    navigateTo("/fake-product");
  };

  const handleBack = () => {
    navigate(-1);
  };

  return (
    <Box
      sx={{
        backgroundImage: `url(${bgImg})`,
        minHeight: "80vh",
        backgroundRepeat: "no-repeat",
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundSize: "cover",
        zIndex: -2,
        overflowY: "scroll",
      }}
    >
      <Paper
        elevation={3}
        sx={{
          width: "400px",
          margin: "auto",
          marginTop: "10%",
          marginBottom: "10%",
          padding: "3%",
          backgroundColor: "#e3eefc",
        }}
      >
        <Box
          sx={{
            textAlign: "center",
            marginBottom: "5%",
          }}
        >
          <Typography
            variant="h2"
            sx={{
              textAlign: "center",
              marginBottom: "3%",
              fontFamily: "Gambetta",
              fontWeight: "bold",
              fontSize: "2.5rem",
            }}
          >
            Scan QR Code
          </Typography>

          <QrScannerComponent passData={passData} />

          <Box
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "center",
            }}
          >
            <Button
              onClick={handleBack}
              sx={{
                marginTop: "5%",
              }}
            >
              Back
            </Button>
          </Box>
        </Box>
      </Paper>
    </Box>
  );
};

export default ScannerPage;
