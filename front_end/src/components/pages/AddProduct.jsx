import { Box, Paper, Typography } from "@mui/material";
import bgImg from "../../img/bg.png";
import { TextField, Button } from "@mui/material";
import { useEffect, useState } from "react";
import axios from "axios";
// import abi from "../../utils/Blocksafe.json";
import QRCode from "qrcode.react";
import dayjs from "dayjs";
import useAuth from "../../hooks/useAuth";
import { useNavigate } from "react-router-dom";
import { fromLatLng, setKey } from "react-geocode";
import toast, { Toaster } from "react-hot-toast";
import { contractABI, contractAddress } from "../../utils/constants";

import { ethers } from "ethers";

const notify = () => toast("Product Added");

const getEthereumObject = () => window.ethereum;

const findMetaMaskAccount = async () => {
  try {
    const ethereum = getEthereumObject();

    if (!ethereum) {
      console.error("Make sure you have Metamask!");
      alert("Make sure you have Metamask!");
      return null;
    }

    console.log("We have the Ethereum object", ethereum);
    const accounts = await ethereum.request({ method: "eth_accounts" });

    if (accounts.length !== 0) {
      const account = accounts[0];
      console.log("Found an authorized account:", account);
      return account;
    } else {
      console.error("No authorized account found");
      return null;
    }
  } catch (error) {
    console.error(error);
    return null;
  }
};

const AddProduct = () => {
  const [currentAccount, setCurrentAccount] = useState("");
  const [serialNumber, setSerialNumber] = useState("");
  const [name, setName] = useState("");
  const [brand, setBrand] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState({
    file: [],
    filepreview: null,
  });
  const [qrData, setQrData] = useState("");
  const [manuDate, setManuDate] = useState("");
  const [manuLatitude, setManuLatitude] = useState("");
  const [manuLongtitude, setManuLongtitude] = useState("");
  const [manuName, setManuName] = useState("");
  const [loading, setLoading] = useState("");
  const [manuLocation, setManuLocation] = useState("");
  const [isUnique, setIsUnique] = useState(true);

  const { auth } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    findMetaMaskAccount().then((account) => {
      if (account !== null) {
        setCurrentAccount(account);
      }
    });
  }, []);

  useEffect(() => {
    if (auth && auth.user) {
      getUsername();
    }
    getCurrentTimeLocation();
  }, [auth]);

  //TODO add the geocode API key
  useEffect(() => {
    setKey("AIzaSyB5MSbxR9Vuj1pPeGvexGvQ3wUel4znfYY");
    fromLatLng(manuLatitude, manuLongtitude).then(
      (response) => {
        const address = response.results[0].formatted_address;
        let city, state, country;
        for (
          let i = 0;
          i < response.results[0].address_components.length;
          i++
        ) {
          for (
            let j = 0;
            j < response.results[0].address_components[i].types.length;
            j++
          ) {
            switch (response.results[0].address_components[i].types[j]) {
              case "locality":
                city = response.results[0].address_components[i].long_name;
                break;
              case "administrative_area_level_1":
                state = response.results[0].address_components[i].long_name;
                break;
              case "country":
                country = response.results[0].address_components[i].long_name;
                break;
              default:
                break;
            }
          }
        }
        setManuLocation(address.replace(/,/g, ";"));
        console.log("city, state, country: ", city, state, country);
        console.log("address:", address);
      },
      (error) => {
        console.error(error);
      }
    );
  }, [manuLatitude, manuLongtitude]);

  const generateQRCode = async (serialNumber) => {
    const data = contractAddress + "," + serialNumber;
    setQrData(data);
    console.log("QR Code: ", qrData);
  };

  const downloadQR = () => {
    try {
      const canvas = document.getElementById("QRCode");
      if (!canvas) {
        console.error("Qr code not found");
        return;
      }
      const pngUrl = canvas
        .toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
      let downloadLink = document.createElement("a");
      downloadLink.href = pngUrl;
      downloadLink.download = `${serialNumber}.png`;
      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);
    } catch (error) {
      console.log("Error downloading qrcode");
    }
  };

  const handleBack = () => {
    navigate(-1);
  };

  const handleImage = async (e) => {
    setImage({
      ...image,
      file: e.target.files[0],
      filepreview: URL.createObjectURL(e.target.files[0]),
    });
  };

  const getUsername = async (e) => {
    if (!auth || !auth.user) return;

    try {
      const res = await axios.get(`http://localhost:5000/profile/${auth.user}`);
      setManuName(res?.data[0]?.name || "");
    } catch (error) {
      console.error("Error fetching username:", error);
    }
  };

  //to upload image
  const uploadImage = async (image) => {
    const data = new FormData();
    data.append("image", image.file);

    axios
      .post("http://localhost:5000/upload/product", data, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        console.log(res);

        if (res.data.success === 1) {
          console.log("image uploaded");
        }
      });
  };

  //registering product
  const registerProduct = async () => {
    if (!serialNumber || !name || !brand || !description || !image.file) {
      console.error("Please fill in all fields");
      return;
    }

    const ethereum = getEthereumObject();
    if (!ethereum) {
      console.error("Metamask not detected");
      return;
    }

    const provider = new ethers.providers.Web3Provider(ethereum);
    const signer = provider.getSigner();
    const contract = new ethers.Contract(contractAddress, contractABI, signer);

    try {
      const txn = await contract.registerProduct(
        name,
        brand,
        serialNumber,
        description,
        image.file.name,
        manuName,
        manuLocation,
        dayjs().format("YYYY-MM-DD HH:mm:ss")
      );

      setLoading(true);
      await txn.wait();
      setLoading(false);

      await uploadImage(image);
      await generateQRCode(serialNumber);
      notify();
    } catch (error) {
      console.error("Error registering product:", error);
      setLoading(false);
    }
  };

  const getCurrentTimeLocation = () => {
    setManuDate(dayjs().unix());
    navigator.geolocation.getCurrentPosition(function (position) {
      setManuLatitude(position.coords.latitude);
      setManuLongtitude(position.coords.longitude);
    });
  };

  //adding product to the database
  const addProductDB = async (e) => {
    e.preventDefault();
    try {
      const profileData = JSON.stringify({
        serialNumber: serialNumber,
        name: name,
        brand: brand,
      });

      const res = await axios.post(
        "http://localhost:5000/addproduct",
        profileData,
        {
          headers: { "Content-Type": "application/json" },
        }
      );

      console.log(JSON.stringify(res.data));
    } catch (err) {
      console.log(err);
    }
  };

  const checkUnique = async () => {
    const res = await axios.get("http://localhost:5000/product/serialNumber");

    const existingSerialNumbers = res.data.map(
      (product) => product.serialnumber
    );
    existingSerialNumbers.push(serialNumber);

    // checking for duplicated serial number
    const duplicates = existingSerialNumbers.filter(
      (item, index) => existingSerialNumbers.indexOf(item) !== index
    );
    console.log("duplicates: ", duplicates);
    const isDuplicate = duplicates.length >= 1;

    setIsUnique(!isDuplicate);
    console.log(existingSerialNumbers);
    console.log("isUnique: ", isUnique);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    checkUnique();

    if (isUnique) {
      uploadImage(image);
      addProductDB(e); // add product to database
      setLoading(
        "Please pay the transaction fee to update the product details..."
      );
      await registerProduct(e);
    }

    setIsUnique(true);
  };

  return (
    <Box
      sx={{
        backgroundImage: `url(${bgImg})`,
        minHeight: "80vh",
        backgroundRepeat: "no-repeat",
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundSize: "cover",
        zIndex: -2,
        overflowY: "scroll",
      }}
    >
      <Paper
        elevation={3}
        sx={{
          width: "400px",
          margin: "auto",
          marginTop: "10%",
          marginBottom: "10%",
          padding: "3%",
          backgroundColor: "#F2E6D0",
        }}
      >
        <Typography
          variant="h2"
          sx={{
            textAlign: "center",
            marginBottom: "3%",
            fontFamily: "Gambetta",
            fontWeight: "bold",
            fontSize: "2.5rem",
          }}
        >
          Add Product
        </Typography>
        <form onSubmit={handleSubmit}>
          <TextField
            fullWidth
            error={!isUnique}
            helperText={!isUnique ? "Serial Number already exists" : ""}
            id="outlined-basic"
            margin="normal"
            label="Serial Number"
            variant="outlined"
            inherit="False"
            onChange={(e) => setSerialNumber(e.target.value)}
            value={serialNumber}
          />

          <TextField
            fullWidth
            id="outlined-basic"
            margin="normal"
            label="Name"
            variant="outlined"
            inherit="False"
            onChange={(e) => setName(e.target.value)}
            value={name}
          />

          <TextField
            fullWidth
            id="outlined-basic"
            margin="normal"
            label="Brand"
            variant="outlined"
            inherit="False"
            onChange={(e) => setBrand(e.target.value)}
            value={brand}
          />

          <TextField
            fullWidth
            id="outlined-basic"
            margin="normal"
            label="Description"
            variant="outlined"
            inherit="False"
            multiline
            minRows={2}
            onChange={(e) => setDescription(e.target.value)}
            value={description}
          />

          <Button
            variant="outlined"
            component="label"
            fullWidth
            sx={{
              marginTop: "3%",
              marginBottom: "3%",
              color: "#F1B749",
              borderColor: "#F1B749",
              "&:hover": { color: "#F1B749", borderColor: "#F1B749" },
            }}
          >
            Upload Image
            <input type="file" hidden onChange={handleImage} />
          </Button>

          {image.filepreview !== null ? (
            <img
              src={image.filepreview}
              alt="preview"
              style={{ width: "100%", height: "100%" }}
            />
          ) : null}

          {qrData !== "" && (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                marginTop: "3%",
              }}
            >
              <QRCode value={qrData} id="QRCode" renderAs="canvas" />
            </div>
          )}

          {qrData !== "" && (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                marginTop: "3%",
              }}
            >
              <Button
                variant="outlined"
                component="label"
                fullWidth
                sx={{ marginTop: "3%", marginBottom: "3%" }}
                onClick={downloadQR}
              >
                Download
              </Button>
            </div>
          )}

          {loading && (
            <Typography
              variant="body2"
              sx={{
                textAlign: "center",
                marginTop: "3%",
              }}
            >
              {String(loading)}
            </Typography>
          )}

          <Button
            variant="contained"
            type="submit"
            sx={{
              width: "100%",
              marginTop: "3%",
              backgroundColor: "#F1B749",
              "&:hover": { backgroundColor: "#F1B749" },
            }}
            onClick={getCurrentTimeLocation}
          >
            Add Product
          </Button>

          <Box
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "center",
            }}
          >
            <Button
              onClick={handleBack}
              sx={{
                marginTop: "5%",
                color: "#F1B749",
              }}
            >
              Back
            </Button>
          </Box>
        </form>
      </Paper>
      <Toaster />
    </Box>
  );
};

export default AddProduct;
