import React, { useState } from "react";
import QrScanner from "react-qr-scanner";

const QrScannerComponent = ({ passData }) => {
  const [scannedData, setScannedData] = useState("");

  const handleError = (error) => {
    console.error("QR code detection error:", error);
  };

  const handleScan = (data) => {
    if (data) {
      setScannedData(data);
      passData(data);
    }
  };

  return (
    <QrScanner
      delay={300}
      onError={handleError}
      onScan={handleScan}
      style={{ width: "100%" }}
    />
  );
};

export default QrScannerComponent;
