// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.17;

contract Blocksafe {
    address public owner;

    struct ProductHistory {
        uint id;
        string actor;
        string location;
        string timestamp;
        bool isSold;
    }

    struct Product {
        string name;
        string serialNumber;
        string description;
        string brand;
        string image;
        uint historySize;
        mapping(uint => ProductHistory) history;
    }

    mapping(string => Product) products;

    constructor() {
        owner = msg.sender;
    }

    function registerProduct(
        string memory _name,
        string memory _brand,
        string memory _serialNumber,
        string memory _description,
        string memory _image,
        string memory _actor,
        string memory _location,
        string memory _timestamp
    ) public {
        require(
            bytes(products[_serialNumber].serialNumber).length == 0,
            "Product already exists"
        );

        Product storage p = products[_serialNumber];
        p.name = _name;
        p.brand = _brand;
        p.serialNumber = _serialNumber;
        p.description = _description;
        p.image = _image;
        p.historySize = 0;

        addProductHistory(_serialNumber, _actor, _location, _timestamp, false);
    }

    function addProductHistory(
        string memory _serialNumber,
        string memory _actor,
        string memory _location,
        string memory _timestamp,
        bool _isSold
    ) public {
        Product storage p = products[_serialNumber];
        p.historySize++;
        p.history[p.historySize] = ProductHistory(
            p.historySize,
            _actor,
            _location,
            _timestamp,
            _isSold
        );
    }

    function getProduct(
        string memory _serialNumber
    )
        public
        view
        returns (
            string memory,
            string memory,
            string memory,
            string memory,
            string memory,
            uint,
            ProductHistory[] memory
        )
    {
        Product storage product = products[_serialNumber];
        ProductHistory[] memory pHistory = new ProductHistory[](
            product.historySize
        );

        for (uint i = 0; i < product.historySize; i++) {
            pHistory[i] = product.history[i + 1];
        }

        return (
            product.serialNumber,
            product.name,
            product.brand,
            product.description,
            product.image,
            product.historySize,
            pHistory
        );
    }
}
