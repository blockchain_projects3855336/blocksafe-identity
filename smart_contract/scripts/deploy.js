const hre = require("hardhat");

const main = async () => {
  const Blocksafe = await hre.ethers.getContractFactory("Blocksafe");
  const blocksafe = await Blocksafe.deploy();
  await blocksafe.deployed();

  console.log("Blocksafe address: ", blocksafe.address);
};

const runMain = async () => {
  try {
    await main();
    process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

runMain();
